import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  profileFrom:FormGroup
  submitted;
  constructor(private fb: FormBuilder) {
  this.profileFrom =this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
       
  });

  }
load(){
  this.profileFrom.setValue({
    name:'siva',
    email:'sivaram@123'
  });
}
 onSubmit() {
  
  this.submitted = true;

        // stop here if form is invalid
        if (this.profileFrom.invalid) {
          return;
      }

      alert('SUCCESS!! :-)')
      this.profileFrom.reset()
}

get f() { 
  return this.profileFrom.controls;
 }

}